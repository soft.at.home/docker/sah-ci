FROM registry.gitlab.com/soft.at.home/docker/base:v1.2.1


ENV VERSION_PREFIX="sah-next_"

## Install base packages
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt install -y \
    alien \
    autoconf \
    autoconf-archive \
    bc \
    clang \
    clang-tools \
    cloc \
    cmake \
    doxygen \
    fakeroot \
    gcc \
    gcovr \
    git \
    graphviz \
    libcmocka-dev \
    liblua5.3-dev \
    libxml2-dev \
    libxslt1-dev \
    lua5.3 \
    luarocks \
    m4 \
    make \
    openssh-client \
    openssh-server \
    plantuml \
    pmccabe \
    python3 \
    python3-pip \
    python3-venv \
    rsync \
    shellcheck \
    tree \
    uncrustify=0.72.0+dfsg1-2 \
    unzip \
    liburiparser-dev \
    vim \
    && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

# Lua fix
RUN cd /usr/include && ln -s -r lua5.3 lua
RUN cd /usr/lib/x86_64-linux-gnu && ln -s -r liblua5.3.so.0.0.0 liblua.so

# install lua packagemanager and some rocks
RUN wget https://luarocks.org/releases/luarocks-3.11.0.tar.gz && \
    tar zxpf luarocks-3.11.0.tar.gz && \
    cd ./luarocks-3.11.0 && \
    ./configure && \
    make build && \
    make install

RUN luarocks install luacov && \
    luarocks install busted && \
    luarocks install luacheck

# Python packages
RUN pip3 install --upgrade pip

RUN pip3 install \
         paramiko \
         pytest \
         pytest-cov \
         pytest-html==2.1.1 \
         pytest-dependency \
         pytest-xdist \
         pylint \
         pyyaml \
         pycodestyle \
         radon \
         mypy \
         GitPython \
         scapy==2.4.5 \
         pep8 \
         pyexpect \
         pyflakes \
         setuptools \
         pytk \
         inotify_simple \
         python-gitlab==3.15.0 \
         junit2html \
         kconfiglib \
         python-jenkins


# Install SoftAtHome debian packages
RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt install -y \
    bison \
    flex \
    libcap-ng-dev \
    libssl-dev \
    && \
    rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*


ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH \
    RUST_VERSION=1.67.0

# add Cargo
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
RUN rustup install $RUST_VERSION
RUN rustup default $RUST_VERSION
RUN rustup target add armv7-unknown-linux-gnueabi
RUN rustup target add aarch64-unknown-linux-gnu
RUN rustup target add mips-unknown-linux-musl


COPY resources/config/ /

RUN export PKG_CONFIG_PATH=/usr/share/pkgconfig:/usr/lib/x86_64-linux-gnu/pkgconfig:/usr/lib/pkgconfig && \
    git clone https://gitlab.com/soft.at.home/logging/libsahtrace.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-ser-odl.git && \
    git clone https://gitlab.com/soft.at.home/pcb/pcb-ser-ddw.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libpcb.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libmtk.git && \
    git clone https://gitlab.com/soft.at.home/pcb/libusermngt.git && \
    export STAGINGDIR=/ && \
    cd libsahtrace && make compile && make install && \
    cd ../libusermngt && make compile && make install && \
    cd ../libpcb && make compile && make install && \
    cd ../libmtk && make compile && make install && \
    cd ../pcb-ser-odl && make compile && make install && \
    cd ../pcb-ser-ddw && make compile && make install && \
    cd .. && rm -rf libsahtrace pcb-ser-ddw libpcb libmtk libusermngt

RUN wget https://github.com/mikefarah/yq/releases/download/v4.44.2/yq_linux_amd64 -O /usr/bin/yq &&\
    chmod +x /usr/bin/yq

RUN cd /tmp

# install sahlab as it is needed by code-quality
RUN git clone -b v1.32.4 https://gitlab.com/prpl-foundation/tooling/sahlab.git ### sahlab
RUN cd sahlab && \
    version=$(git tag --points-at HEAD | tail -n 1 | sed -n 's/.*\(v[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/p') && \
    sed -i "s/version='99.99.99'/version='$version'/g" setup.py && \
    python3 -m pip install --upgrade . && \
    cd ../ && \
    rm -rf sahlab

# install code-quality from the git repo
RUN git clone -b v0.18.0 https://gitlab.com/prpl-foundation/tooling/code-quality.git ### sah-code-quality
RUN cd code-quality && \
    version=$(git tag --points-at HEAD | tail -n 1 | sed -n 's/.*\(v[0-9]\+\.[0-9]\+\.[0-9]\+\).*/\1/p') && \
    sed -i "s/version='99.99.99'/version='$version'/g" setup.py && \
    python3 -m pip install --upgrade . && \
    cd ../ && \
    rm -rf code-quality

# compile amxo-cg and amxo-xml-to from the opensource repo
RUN git clone -b sah-next_v4.11.11 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxb.git ### sah_libamxb
RUN git clone -b v5.3.2 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxo.git ### sah_libamxo
RUN git clone -b sah-next_v0.9.1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxs.git ### sah_libamxs
RUN git clone -b sah-next_v6.7.4 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxd.git ### sah_libamxd
RUN git clone -b sah-next_v2.8.3 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxp.git ### sah_libamxp
RUN git clone -b v1.0.4 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxj.git ### sah_libamxj
RUN git clone -b sah-next_v2.4.1 https://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc.git ### sah_libamxc
RUN git clone -b sah-next_v1.6.4 https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxo-cg.git ### sah_services_amxo-cg
RUN git clone -b v0.2.7 https://gitlab.com/prpl-foundation/components/ambiorix/applications/amxo-xml-to.git ### sah_services_amxo-xml-to
RUN git clone https://github.com/lloyd/yajl

RUN cd yajl && cmake . && make && make install && \
    cd ../libamxc && make && make install && \
    cd ../libamxp && make && make install && \
    cd ../libamxd && make && make install && \
    cd ../libamxj && make && make install && \
    cd ../libamxb && make && make install && \
    cd ../libamxs && make && make install && \
    cd ../libamxo && make && make install && \
    cd ../amxo-cg && make && make install && \
    cd ../amxo-xml-to && make && make install

RUN rm -rf /tmp/libamx*

# Install Ninja from its official releases (to ensure the latest version)
RUN wget https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip && \
    unzip ninja-linux.zip && \
    mv ninja /usr/bin/ninja && \
    chmod +x /usr/bin/ninja && \
    rm ninja-linux.zip

RUN groupadd -g 1122 acl
