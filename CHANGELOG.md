# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v3.3.1 - 2025-02-20(13:14:37 +0000)

## Release v3.3.0 - 2025-02-20(12:55:23 +0000)

### Other

- Amx backend: RPC to reset a plugin statistics
- adapt synt to prpl-foundation
- [Amxc] Add 64 bit versions of macros exposed in variant header
- Improve klocwork reporting at component level

## Release v3.2.2 - 2025-01-24(14:28:09 +0000)

### New

- [Amxs] Add API to monitor search path subscriptions on the client side

### Other

- Custom action handlers must be used when available in creating pcb reply messages
- Avoid variant copies
- Fix parameter identation when saving odl files
- adapt synt to prpl-foundation
- Performance improvements data model transactions
- [Amxs] Add API to monitor search path subscriptions on the client side
- [libamxp] not compiling with libcap-ng 0.8.4
- Re-add verify flag for backwards compatibility
- TR-181i2-latest XML as of 14.01.2025
- Klocwork's staging path does not contain pkgconfig.mk file

## Release v3.2.1 - 2025-01-20(14:44:44 +0000)

### Other

- [CI] jfrog APT key expired

## Release v3.2.0 - 2024-12-17(11:51:10 +0000)

### Other

- Reduce unnecessary formatted writes
- Provide integer -> string conversion functions
- [libamxp]Memory leak can occur when signal can not be emitted
- [Amxc] Enable comparison of null variants
- [amxo-cg] not compiling with gcc13.3.0
- Reduce usage of appendf in expression parser
- Improve data model mamagement functions
- Device2 obsoleted and deprecated datamodel enumeration values should be ignored.
- Cache parsed mib expressions
- [Amxb] Always log bus timeouts to syslog
- Custom action handlers must be used when available in creating pcb reply messages
- [AMX] Slots callback functions are called multiple times

## Release v3.1.5 - 2024-12-05(12:06:18 +0000)

### Other

- [CI] code-style issues are strangely formatted

## Release v3.1.4 - 2024-11-28(12:34:32 +0000)

### Other

- [FSAM][Container] MQTT Events not dispatched on Device.MQTT.Client.
- [Gitlab] Find an alternative to Remove all approvals when commits are added to the source branch

## Release v3.1.3 - 2024-11-25(08:51:00 +0000)

### Other

- amxc_set improvements

## Release v3.1.2 - 2024-11-21(18:11:09 +0000)

### Other

- Failed to load amx pcb backend in amxdev docker

## Release v3.1.1 - 2024-11-19(14:27:02 +0000)

### Other

- Fix sah_nte artifactory issues

## Release v3.1.0 - 2024-11-05(14:07:36 +0000)

### Other

- Optimizations in ambiorix libraries

## Release v3.0.18 - 2024-10-28(12:02:13 +0000)

### Other

- Add ninja to docker image building libxml2 CI

## Release v3.0.17 - 2024-10-25(08:06:24 +0000)

### Other

- Update the tr-181-2-usp-full XML to the latest version.
- [LCM] The first DU instance gets renamed (but not in dm)

## Release v3.0.16 - 2024-10-24(15:12:53 +0000)

## Release v3.0.15 - 2024-10-23(15:07:18 +0000)

### Other

- Update the key validation behaviour.

## Release v3.0.14 - 2024-10-23(11:49:16 +0000)

## Release v3.0.13 - 2024-10-09(08:32:00 +0000)

## Release v3.0.12 - 2024-09-25(13:21:43 +0000)

### Other

- Update the TR-181 XML file with the latest 24.09.2024

## Release v3.0.11 - 2024-09-16(13:53:41 +0000)

### Other


## Release v3.0.10 - 2024-09-10(11:08:46 +0000)

### Other

- - Integrate softathome toolchain for BRCM BSP bcm963xx 6.1.1

## Release v3.0.9 - 2024-08-01(14:04:10 +0000)

### Other

- Fix opensource push

## Release v3.0.8 - 2024-08-01(12:59:52 +0000)

### Other

- [artifactory] fix API kys in plain, rebase will follow
- [CI] ODL check broken - complains about libc

## Release v3.0.7 - 2024-07-25(11:53:01 +0000)

### Other

- - amx-validator: add excluded list for accessType.

## Release v3.0.6 - 2024-07-18(09:25:09 +0000)

### Other

- [USP] Add obuspa license

## Release v3.0.5 - 2024-07-18(08:00:41 +0000)

### Other

- install yq command

## Release v3.0.4 - 2024-07-16(08:20:14 +0000)

### Other

- - amx-validator: missing key checks
- - AMX-Validator: Special Alias key check.
- Add exclude enum capabilities for the amx-validation script.
- Functional key check

## Release v3.0.3 - 2024-07-08(14:24:00 +0000)

### Other

- [CI] Add targets to the build pipeline

## Release v3.0.2 - 2024-07-04(06:36:01 +0000)

### Other

- [CI] Add targets to the build pipeline
- [CI] artifactory explorer misses libwebsockets package

## Release v3.0.1 - 2024-07-02(09:45:53 +0000)

### Other


## Release v3.0.0 - 2024-06-04(09:54:46 +0000)

### Breaking

- - Update the JFrog executable used in Artifactory

### Other

- - [WIFI7] First integration of QSDK ATH 12.4

## Release v2.0.5 - 2024-05-31(09:20:44 +0000)

### Other


## Release v2.0.4 - 2024-05-28(08:39:30 +0000)

### Other


## Release v2.0.3 - 2024-05-28(07:46:56 +0000)

### Other

- ci: make IMAGE_PATH available for opensource

## Release v2.0.2 - 2024-05-28(07:40:01 +0000)

## Release v2.0.1 - 2024-05-27(11:18:26 +0000)

### Other

- Print out total number of errors found

## Release v2.0.0 - 2024-05-21(13:54:06 +0000)

### Breaking

- - Update the Artifactory version for the JFrog executable used in Artifactory

### Other


## Release v1.9.2 - 2024-05-15(16:17:25 +0000)

### Other

- [code-quality] Make it possible to run FixLicenses locally

## Release v1.9.1 - 2024-05-03(13:07:58 +0000)

## Release v1.9.0 - 2024-05-03(11:04:48 +0000)

### Other

- Add tools for performing TR106 syntax rule checks on AMX/ODL files

## Release v1.8.7 - 2024-04-08(07:01:34 +0000)

### Other

- Missing compilation flags during cross compilations

## Release v1.8.6 - 2024-03-26(12:26:11 +0000)

### Other

- [Regression] Old p4 tags in Component.dep no longer supported

## Release v1.8.5 - 2024-03-22(15:52:20 +0000)

## Release v1.8.4 - 2024-03-21(09:52:37 +0000)

## Release v1.8.3 - 2024-03-13(10:30:43 +0000)

### Other

- [CI][services_lua] normalize_build_dir() function tries to move directory symlinks

## Release v1.8.2 - 2024-03-12(12:51:56 +0000)

### Other

- [CI] Fix indentation

## Release v1.8.1 - 2024-03-12(11:55:22 +0000)

### Other

- [CI] Fix: meson dependency of buildutils screws up the CFLAGS and LDFLAGS of opensource compilations

## Release v1.8.0 - 2024-03-07(12:49:33 +0000)

### Other


## Release v1.7.1 - 2024-02-27(18:58:09 +0000)

### Other

- Update lib-pcb-dev to 6.10.10

## Release v1.7.0 - 2024-02-19(10:59:08 +0000)

### Other


## Release v1.6.13 - 2024-02-19(09:51:46 +0000)

### Other

- add test and fix for partials failing the check

## Release v1.6.12 - 2024-02-08(17:11:44 +0000)

### Other

- Find an alternative to push rules

## Release v1.6.11 - 2024-01-16(14:13:02 +0000)

### Other

- Add clearer message for ScanConfigOpts complaint

## Release v1.6.10 - 2024-01-12(12:06:26 +0000)

### Other

- Upstep pcb versions

## Release v1.6.9 - 2023-12-21(13:49:09 +0000)

### Other


## Release v1.6.8 - 2023-12-20(12:34:40 +0000)

### Other


## Release v1.6.7 - 2023-12-13(15:25:11 +0000)

### Other


## Release v1.6.6 - 2023-12-12(15:32:10 +0000)

### Other


## Release v1.6.5 - 2023-12-12(10:22:36 +0000)

### Fixes

- Debug issue sahname packages

## Release v1.6.4 - 2023-11-14(15:28:16 +0000)

### Other


## Release v1.6.3 - 2023-11-14(14:51:24 +0000)

### Other


## Release v1.6.2 - 2023-10-26(14:45:50 +0000)

## Release v1.6.1 - 2023-10-25(10:28:26 +0000)

### Other


## Release v1.6.0 - 2023-10-24(10:21:26 +0000)

### New

- Add generate license on same branch job

## Release v1.5.0 - 2023-10-19(15:17:27 +0000)

### Security

- Update to bullseye-20231009

## Release v1.4.0 - 2023-10-16(14:08:54 +0000)

### Other

- Add CI support for the rust MIPS toolchain

## Release v1.3.21 - 2023-09-29(15:19:07 +0000)

### Other


## Release v1.3.20 - 2023-09-29(10:23:26 +0000)

### Other


## Release v1.3.19 - 2023-09-29(07:24:57 +0000)

### Other


## Release v1.3.18 - 2023-09-22(09:58:35 +0000)

### Other


## Release v1.3.17 - 2023-09-21(20:03:18 +0000)

### Other


## Release v1.3.16 - 2023-09-21(15:53:51 +0000)

### Other

- Modify scantree to allow IncludeNode inside SrcNode

## Release v1.3.15 - 2023-09-20(16:33:27 +0000)

### Other

- Add check for licenses

## Release v1.3.14 - 2023-09-18(09:13:51 +0000)

### Other


## Release v1.3.13 - 2023-09-15(14:29:41 +0000)

### Other

- [scantree] Add support for SubRootNode in SrcNode

## Release v1.3.12 - 2023-09-14(10:25:40 +0000)

### Other

- add gitpython for merge request generation

## Release v1.3.11 - 2023-08-21(12:44:42 +0000)

### Other


## Release v1.3.10 - 2023-08-16(09:40:29 +0000)

### Fixes

- Remove perforce sources

### Other

- [ci] Move cloc package from sah_nte to sah_test_ci

## Release v1.3.9 - 2023-07-24(14:19:49 +0000)

## Release v1.3.8 - 2023-06-23(07:43:28 +0000)

### Other

- Add libyajl-dev explicitly

## Release v1.3.7 - 2023-06-23(07:25:27 +0000)

### Other

- [CI] Replace CI_BUILD_REF_SLUG with CI_COMMIT_REF_SLUG
- Resolve errors from new Pylint version

## Release v1.3.6 - 2023-05-10(12:39:27 +0000)

### Other


## Release v1.3.5 - 2023-05-08(09:24:07 +0000)

### Other

- Modify remove_file_if_exists to work on directories
- Fix and optimize script used for filling the artifactory with packages for new cross-compilers

## Release v1.3.4 - 2023-04-20(13:51:54 +0000)

### Fixes

- Create acl group with different group id

## Release v1.3.3 - 2023-02-28(11:27:39 +0000)

### Other

- Allow `.uc` files in the odl directory
- Disable pylint

## Release v1.3.2 - 2023-02-10(16:48:42 +0000)

### New

- Add armv7 and aarch64 for cargo

## Release v1.3.1 - 2023-02-08(18:16:00 +0000)

### Other

- Add gitlabinternal to known_hosts

## Release v1.3.0 - 2023-02-08(12:19:00 +0000)

### New

- Add RUST Support in CI

## Release v1.2.24 - 2022-12-02(15:13:18 +0000)

## Release v1.2.23 - 2022-12-02(11:48:17 +0000)

## Release v1.2.22 - 2022-10-26(09:23:58 +0000)

## Release v1.2.21 - 2022-10-12(09:34:00 +0000)

### Other

- Add acl group to container

## Release v1.2.20 - 2022-10-10(14:40:46 +0000)

### Other

- Add autoconf-archive

## Release v1.2.19 - 2022-10-06(07:25:45 +0000)

### Fixes

- Update sah-pcb-docgen-dev to version 3.9.7

## Release v1.2.18 - 2022-08-25(14:17:13 +0000)

## Release v1.2.17 - 2022-08-24(10:18:03 +0000)

## Release v1.2.16 - 2022-08-24(09:50:39 +0000)

### Other

- odl-check job fails due to empty odl file

## Release v1.2.15 - 2022-07-11(11:20:49 +0000)

### Other

- USP needs async userflags for functions

## Release v1.2.14 - 2022-06-29(06:46:46 +0000)

## Release v1.2.13 - 2022-06-14(12:33:53 +0000)

### Other

- Add cmake to container

## Release v1.2.12 - 2022-05-10(12:22:41 +0000)

### Other

- Update to latest opensource pcb versions

## Release v1.2.11 - 2022-05-06(14:48:12 +0000)

### Fixes

- revert dependency problem

## Release v1.2.10 - 2022-04-29(12:28:26 +0000)

### Other

- Upstep pcb versions

## Release v1.2.9 - 2022-03-28(09:58:23 +0000)

### Fixes

- Installing deps of deps can result in wrong versions

## Release v1.2.7 - 2022-03-02(10:38:21 +0000)

### Other

- Upstep sah-code-quality to v0.9.3

## Release v1.2.6 - 2022-02-21(09:44:54 +0000)

### Fixes

- Remove amxo-cg and amxo-xml-to

## Release v1.2.5 - 2022-02-03(15:50:39 +0000)

### Other

- - No associated bugs found

## Release v1.2.4 - 2022-02-01(15:04:39 +0000)

### Fixes

- Correct STAGINGDIR location

## Release v1.2.3 - 2022-02-01(10:02:21 +0000)

